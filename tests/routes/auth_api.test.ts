import { loginWelcomeMessage, registerWelcomeMessage } from '../../src/core/constants';
import App from '../../src/app';
import request from 'supertest';
import express from 'express';
describe('Auth endpoints', () => {

    let server: express.Application;

    let phoneNumber = '0957698503';
    let password = '123456';
    let username = "abdalrahman hatahet";
    let type = "Admin";

    beforeAll(async () => {
        server = await App.getInstance().server;
    })

    afterAll(async () => {
        App.getInstance().disconnect();
    })


    describe('register endpoint', () => {

        it('gives success response when called with valid data', async () => {

            let response = await request(server)
                .post('/auth/register')
                .send({
                    name: username,
                    phone_number: phoneNumber,
                    password: password,
                    type: type
                })
            expect(response.ok).toEqual(true);
            expect(response.body).toEqual({ success: true, message: registerWelcomeMessage, data: {} });
        })

    })

    describe('login endpoint', () => {

        it('login the registered user successfully', async () => {


            let response = await request(server)
                .post('/auth/login')
                .send({
                    phone_number: phoneNumber,
                    password: password,
                });

            expect(response.ok).toEqual(true);
            expect(response.body).toEqual({ success: true, message: loginWelcomeMessage, data: {} });

        })
    })
});