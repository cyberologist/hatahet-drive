import '../../src/core/loaders/LoadEnv'; // Must be the first import
import { AddressDTO } from './../../src/infrastructure/dtos/address';
import { UserDTO } from './../../src/infrastructure/dtos/user';
import { UserDao } from '../../src/infrastructure/daos/user.dao';
import mongoosLoader from '../../src/core/loaders/mongoose';
import { encryptPassword, equalEncrypted } from '../../src/core/utils/functions';
import { User, UserType } from '../../src/domain/entities/User';
import { UserAlreadyExistsFailure, UserNotFoundFailure } from '../../src/domain/failures/user_failures';
import mongoose from 'mongoose';
import App from '../../src/app';
describe('user dao', () => {

    let userDao: UserDao;

    beforeAll(async () => {
        await App.getInstance().connectMongoose();
        userDao = new UserDao();
    });

    beforeEach(async () => {
        (await App.getInstance().dbConnection).dropDatabase();
    })
    afterAll(async () => {
        App.getInstance().disconnect();
    })

    it('creates a new user successfully and returns it', async () => {
        let toCreateUser = new UserDTO({
            id: '1',
            name: 'abdalrahman',
            phoneNumber: '09123456789',
            type: UserType.Admin,
            pwdhash: await encryptPassword('123'),
            image: undefined,
            address: new AddressDTO({ lat: 3123.42, long: 32.42 })
        }

        );
        let user: UserDTO | UserAlreadyExistsFailure = await userDao.craeteUser(toCreateUser);

        expect(user).toBeInstanceOf(UserDTO);
        expect((user as User).name).toBe('abdalrahman');
        expect((user as User).phoneNumber).toBe('09123456789');
        expect((user as User).type).toBe(UserType.Admin);
        expect(equalEncrypted('123', (user as User).pwdhash)).toBeTruthy();

    });
    it('return UserAlreadyExistsFailure according to phoneNumber', async () => {
        let toCreateUser = new UserDTO({
            id: '1',
            name: 'abdalrahman',
            phoneNumber: '09123456789',
            type: UserType.Admin,
            pwdhash: await encryptPassword('123')
        }
        );

        await userDao.craeteUser(toCreateUser);

        let toCreateUser2 = new UserDTO({
            id: '1',
            name: 'mohammad',
            phoneNumber: '09123456789',
            type: UserType.Standard,
            pwdhash: await encryptPassword('456')
        }
        );
        let failure: UserDTO | UserAlreadyExistsFailure = await userDao.craeteUser(toCreateUser2);

        expect(failure).toBeInstanceOf(UserAlreadyExistsFailure);

    });

    it('reads existing user by phoneNumber', async () => {
        let toCreateUser: User = new UserDTO({
            id: '1',
            name: 'mohammad',
            phoneNumber: '09123456789',
            type: UserType.Standard,
            pwdhash: await encryptPassword('456')
        })

        let user: UserDTO | UserAlreadyExistsFailure = await userDao.craeteUser(toCreateUser);

        let foundUser: UserDTO | UserNotFoundFailure = await userDao.readUser('09123456789');
        expect(foundUser).toBeInstanceOf(UserDTO);

        expect(foundUser).toEqual(user);
        expect((foundUser as User).phoneNumber).toEqual((user as User).phoneNumber);

    });


    it('reads UserNotFoundFailure when a user with the entered phoneNumber not found', async () => {
        let failure: User | UserNotFoundFailure = await userDao.readUser('09123456789');
        expect(failure).toBeInstanceOf(UserNotFoundFailure);

    });

    it('updates existing user', async () => {

        let toCreateUser: User = new UserDTO({
            id: '1',
            name: 'abdalrahman',
            phoneNumber: '09123456789',
            type: UserType.Admin,
            pwdhash: await encryptPassword('123')
        });

        let user: UserDTO | UserAlreadyExistsFailure = await userDao.craeteUser(toCreateUser);

        let updatedUser: UserDTO | UserAlreadyExistsFailure = await userDao.updateUser(
            '09123456789', {
            name: 'mohammad',
            phoneNumber: '0957698503',
            pwdhash: await encryptPassword('789'),
            image: 'image',
        });

    })

    it('deletes users by phoneNumber', async () => {

        let toCreateUser: User = new UserDTO({
            id: '1',
            name: 'abdalrahman',
            phoneNumber: '09123456789',
            type: UserType.Admin,
            pwdhash: await encryptPassword('123')
        });

        let user: UserDTO | UserAlreadyExistsFailure = await userDao.craeteUser(toCreateUser);

        let deletedUser: UserDTO | UserAlreadyExistsFailure = await userDao.deleteUser('09123456789');

        let userAfterDeletion: UserDTO | UserNotFoundFailure = await userDao.readUser('09123456789');

        expect(userAfterDeletion).toBeInstanceOf(UserNotFoundFailure);
        expect(deletedUser).toBeInstanceOf(UserDTO);
        expect(deletedUser).toEqual(user);

    })

});