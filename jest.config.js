module.exports = {
    "roots": [
        "<rootDir>",
        "<rootDir>/src"
    ],
    "setupFiles": ["<rootDir>/src/core/loaders/LoadEnv.ts"],

    "testMatch": [
        "**/__tests__/**/*.+(ts|tsx|js)",
        "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
    "moduleDirectories": ['node_modules', 'src'],
    "testEnvironment": "node",
    "coveragePathIgnorePatterns": [
        "/node_modules/"
    ]
}
