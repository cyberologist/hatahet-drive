import { FileDTO } from './../../infrastructure/dtos/file';
import { inject, injectable } from 'inversify';
import { Folder } from 'src/domain/entities/Folder';
import { IFolderDao } from 'src/domain/facades/i_folder.dao';
import { TYPES } from '../../core/types';
import { Failure } from '../../domain/failures/failure';
import { IUsecase } from './usecase';


@injectable()
export class GetFolderChildren implements IUsecase<GetFolderChildrenParams, (Folder | FileDTO)[]> {

    private folderDao: IFolderDao;

    constructor(@inject(TYPES.FOLDER_DAO) folderDao: IFolderDao) {
        this.folderDao = folderDao;
    }

    async execute(params: GetFolderChildrenParams): Promise<(Folder | FileDTO)[] | Failure> {
        return await this.folderDao.getChildren(
            params.folderId,
            params.pageNumber
        );
    }

}
export class GetFolderChildrenParams {
    public folderId: string;
    public pageNumber: number;

    constructor(
        folderId: string,
        pageNumber?: number,
    ) {
        this.folderId = folderId;
        this.pageNumber = pageNumber ?? 0;
    }
}