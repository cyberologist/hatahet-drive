import { inject, injectable } from 'inversify';
import { Folder } from 'src/domain/entities/Folder';
import { IFolderDao } from 'src/domain/facades/i_folder.dao';
import { TYPES } from '../../core/types';
import { Failure } from '../../domain/failures/failure';
import { IUsecase } from './usecase';


@injectable()
export class DeleteFolder implements IUsecase<DeleteFolderParams, Folder> {

    private folderDao: IFolderDao;

    constructor(@inject(TYPES.FOLDER_DAO) folderDao: IFolderDao) {
        this.folderDao = folderDao;
    }

    async execute(params: DeleteFolderParams): Promise<Folder | Failure> {
        return await this.folderDao.deleteFolder(
            params.folderId,
        );
    }

}
export class DeleteFolderParams {
    public folderId: string;

    constructor(
        folderId: string,
    ) {
        this.folderId = folderId;
    }
}