import { inject, injectable } from 'inversify';
import { File } from 'src/domain/entities/File';
import { IFileDao } from 'src/domain/facades/i_file.dao';
import { TYPES } from '../../core/types';
import { Failure } from '../../domain/failures/failure';
import { IUsecase } from './usecase';


@injectable()
export class CreateFile implements IUsecase<CreateFileParams, File> {

    private fileDao: IFileDao;

    constructor(@inject(TYPES.FILE_DAO) fileDao: IFileDao) {
        this.fileDao = fileDao;
    }

    async execute(params: CreateFileParams): Promise<File | Failure> {
        return await this.fileDao.createFile(
            params.parentId,
            params.name,
            params.url,
        );
    }

}
export class CreateFileParams {
    public name: string;
    public url: string;
    public parentId: number;

    constructor(
        name: string,
        url: string,
        parentId?: number,
    ) {
        this.name = name;
        this.parentId = parentId ?? 0;
        this.url = url;
    }
}