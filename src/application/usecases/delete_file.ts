import { inject, injectable } from 'inversify';
import { IFileDao } from 'src/domain/facades/i_file.dao';
import { TYPES } from '../../core/types';
import { Failure } from '../../domain/failures/failure';
import { IUsecase } from './usecase';
import { File } from '../../domain/entities/File';

@injectable()
export class DeleteFile implements IUsecase<DeleteFileParams, File> {

    private fileDao: IFileDao;

    constructor(@inject(TYPES.FOLDER_DAO) fileDao: IFileDao) {
        this.fileDao = fileDao;
    }

    async execute(params: DeleteFileParams): Promise<File | Failure> {
        return await this.fileDao.deleteFile(
            params.fileId,
        );
    }

}
export class DeleteFileParams {
    public fileId: number;

    constructor(
        fileId: number,
    ) {
        this.fileId = fileId;
    }
}