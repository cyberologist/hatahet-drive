import { inject, injectable } from 'inversify';
import { TYPES } from '../../core/types';
import { encryptPassword } from '../../core/utils/functions';
import { User, UserType } from '../../domain/entities/User';
import { IUserDao } from '../../domain/facades/i_user.dao';
import { Failure } from '../../domain/failures/failure';
import { IUsecase } from './usecase';


@injectable()
export class RegisterUser implements IUsecase<RegisterUserParams, User> {

    private userService: IUserDao;

    constructor(@inject(TYPES.USER_DAO) userDao: IUserDao) {
        this.userService = userDao;
    }

    async execute(params: RegisterUserParams): Promise<User | Failure> {
        return await this.userService.craeteUser({
            id: '0',
            name: params.name,
            phoneNumber: params.phoneNumber,
            type: params.type,
            pwdhash: await encryptPassword(params.password)
        });
    }

}
export class RegisterUserParams {
    public name: string;
    public phoneNumber: string;
    public type: UserType;
    public password: string;

    constructor(
        name: string,
        phoneNumber: string,
        type: string,
        password: string,

    ) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.type = type == "Admin" ?
            UserType.Admin : type == "Owner" ?
                UserType.Owner : UserType.Standard;
        this.password = password;
    }
}