import { inject, injectable } from 'inversify';
import { Folder } from 'src/domain/entities/Folder';
import { IFolderDao } from 'src/domain/facades/i_folder.dao';
import { TYPES } from '../../core/types';
import { encryptPassword } from '../../core/utils/functions';
import { User, UserType } from '../../domain/entities/User';
import { IUserDao } from '../../domain/facades/i_user.dao';
import { Failure } from '../../domain/failures/failure';
import { IUsecase } from './usecase';


@injectable()
export class CreateFolder implements IUsecase<CreateFolderParams, Folder> {

    private folderDao: IFolderDao;

    constructor(@inject(TYPES.FOLDER_DAO) folderDao: IFolderDao) {
        this.folderDao = folderDao;
    }

    async execute(params: CreateFolderParams): Promise<Folder | Failure> {
        return await this.folderDao.createFolder(
            params.name,
            params.owner.id,
            params.parentId,
        );
    }

}
export class CreateFolderParams {
    public owner: User;
    public name: string;
    public parentId: string;

    constructor(
        owner: User,
        name: string,
        parentId?: string,
    ) {
        this.owner = owner;
        this.name = name;
        this.parentId = parentId ?? '0';
    }
}