import { inject, injectable } from 'inversify';
import { TYPES } from '../../core/types';
import { equalEncrypted } from '../../core/utils/functions';
import { User } from '../../domain/entities/User';
import { IUserDao } from '../../domain/facades/i_user.dao';
import { Failure } from '../../domain/failures/failure';
import { UserCredintialsFailure } from '../../domain/failures/user_failures';
import { IUsecase } from './usecase';

@injectable()
export class LoginUser implements IUsecase<LoginUserParams, User> {

    private userService: IUserDao;

    constructor(@inject(TYPES.USER_DAO) userService: IUserDao) {
        this.userService = userService;
    }

    async execute(params: LoginUserParams): Promise<User | Failure> {
        let result = await this.userService.readUser(params.phoneNumber);
        if (result instanceof User) {
            if (equalEncrypted(result.pwdhash, params.password)) {
                return result;
            } else {
                return new UserCredintialsFailure();
            }
        } else {
            return result;
        }
    }

}
export class LoginUserParams {
    public phoneNumber: string;
    public password: string;

    constructor(
        phoneNumber: string,
        password: string,

    ) {
        this.phoneNumber = phoneNumber;
        this.password = password;
    }
}