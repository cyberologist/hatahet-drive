import { Failure } from 'src/domain/failures/failure';

export interface IUsecase<P, R> {
    execute(params: P): Promise<Failure | R>;
}