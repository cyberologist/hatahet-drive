import { Address } from '../entities/Address';
import { User, UserType } from '../entities/User';
import { UserAlreadyExistsFailure, UserNotFoundFailure, UserValueNotAvailableFailure } from '../failures/user_failures';
export interface IUserDao {

    craeteUser(user: User): Promise<User | UserAlreadyExistsFailure>;

    readUser(phoneNumber: string): Promise<User | UserNotFoundFailure>;

    updateUser(
        userPhoneNumber: string,
        newInfo: UserUpdateParams,
    ): Promise<User | UserNotFoundFailure | UserValueNotAvailableFailure>;

    deleteUser(phoneNumber: string): Promise<User | UserNotFoundFailure>;
}
export interface UserUpdateParams {
    name?: string,
    phoneNumber?: string,
    type?: UserType,
    pwdhash?: string,
    address?: Address,
    image?: string
}