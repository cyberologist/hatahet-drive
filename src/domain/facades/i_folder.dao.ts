import {
    FolderNameAlreadyExistsFailure,
    FolderNotEmptyFailure,
    FolderNotFoundFailure,
    ParentFolderNotFoundFailure
} from '../failures/folder_failures';
import { Folder } from '../entities/Folder';
import { File } from '../entities/File';


export interface IFolderDao {
    createFolder(name: string, ownerId: string, parentId: string):
        Promise<Folder | FolderNameAlreadyExistsFailure | ParentFolderNotFoundFailure>;
    deleteFolder(folderId: string):
        Promise<Folder | FolderNotEmptyFailure | FolderNotFoundFailure>;

    getChildren(folderId: string, pageNumber: number): Promise<(FolderNotFoundFailure | (File | Folder)[])>;
}