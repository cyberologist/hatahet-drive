import { FileNameAlreadyExistsFailure, FileNotFoundFailure, ParentFolderNotFoundFailure } from '../failures/file_failures';
import { File } from '../entities/File';
export interface IFileDao {
    createFile(parentId: number, name: string, url: string):
        Promise<File | FileNameAlreadyExistsFailure | ParentFolderNotFoundFailure>;
    deleteFile(folderId: number):
        Promise<File | FileNotFoundFailure>;
}