
export class Folder {
    id: string;
    name: string;
    parentId: number;
    constructor(id: string, name: string, parentId: number) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

}