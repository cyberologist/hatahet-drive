import { IClientData } from 'src/application/services/JwtService';
import { Address } from './Address';
export enum UserType {
    Admin = 0,
    Owner = 1,
    Standard = 2
}
export class User implements IClientData {
    id: string;
    name: string;
    phoneNumber: string;
    type: UserType;
    pwdhash: string;
    image?: string;
    address?: Address;
    constructor(
        id: string,
        name: string,
        phoneNumber: string,
        type: UserType,
        pwdhash: string,
        image?: string,
        address?: Address,
    ) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.pwdhash = pwdhash;
        this.image = image;
        this.address = address;
    }
}