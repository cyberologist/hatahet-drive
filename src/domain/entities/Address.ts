export class Address {
    id?: string;
    lat?: number;
    long?: number;
    county?: string;
    area?: string;
    subArea?: string;
    street?: string;

    constructor(
        id?: string,
        lat?: number,
        long?: number,
        county?: string,
        area?: string,
        subArea?: string,
        street?: string,
    ) {
        this.id = id;
        this.lat = lat;
        this.long = long;
        this.county = county;
        this.area = area;
        this.subArea = subArea;
        this.street = street;
    }
}
