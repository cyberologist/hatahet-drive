
export class File {
    id: string;
    parentId: number;
    name: string;
    url: string;
    constructor(id: string, parentId: number, name: string, url: string) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.url = url;
    }

}