import { Failure } from './failure';
import { StatusCodes } from 'http-status-codes';

export class FolderNameAlreadyExistsFailure implements Failure {
    statusCode: number = StatusCodes.CONFLICT;
    message: string = 'a folder with the same name as provided name already exists';
}

export class ParentFolderNotFoundFailure implements Failure {
    statusCode: number = StatusCodes.NOT_FOUND;
    message: string = 'can\'t find parent folder';
}
export class FolderNotFoundFailure implements Failure {
    statusCode: number = StatusCodes.NOT_FOUND;
    message: string = 'can\'t find folder';
}
export class FolderNotEmptyFailure implements Failure {
    statusCode: number = StatusCodes.PRECONDITION_FAILED;
    message: string = 'can\'t delete non-empty folder';
}

