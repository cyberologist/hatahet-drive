import { Failure } from './failure';
import { StatusCodes } from 'http-status-codes';

export class FileNameAlreadyExistsFailure implements Failure {
    statusCode: number = StatusCodes.CONFLICT;
    message: string = 'a file with the same name as provided name already exists';
}

export class ParentFolderNotFoundFailure implements Failure {
    statusCode: number = StatusCodes.NOT_FOUND;
    message: string = 'can\'t find parent folder';
}
export class FileNotFoundFailure implements Failure {
    statusCode: number = StatusCodes.NOT_FOUND;
    message: string = 'can\'t find file';
}

