export abstract class Failure {
    statusCode!: number;
    message!: string;
}