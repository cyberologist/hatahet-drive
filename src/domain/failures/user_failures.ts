import { Failure } from './failure';
import { StatusCodes } from 'http-status-codes';

export class UserAlreadyExistsFailure implements Failure {

    statusCode: number = StatusCodes.CONFLICT;
    message: string = 'already have an account , try to login instead';

}
export class UserNotFoundFailure implements Failure {

    statusCode: number = StatusCodes.NOT_FOUND;
    message: string = 'Can\'t find a user with specified informations';

}
export class UserValueNotAvailableFailure implements Failure {

    statusCode: number = StatusCodes.BAD_REQUEST;
    message: string;
    constructor(valueName: String) {
        this.message
            = `provided value for ${valueName} not available`;
    }

}
export class UserCredintialsFailure implements Failure {

    statusCode: number = StatusCodes.UNAUTHORIZED;
    message: string = 'please check your information';

}