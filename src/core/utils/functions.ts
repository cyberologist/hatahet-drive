import logger from './Logger';
import bcrypt from 'bcrypt';
import { pwdSaltRounds } from '../constants';

export const pErr = (err: Error) => {
    if (err) {
        logger.error(err);
    }

};

// TODO split to files 
export const getRandomInt = () => {
    return Math.floor(Math.random() * 1_000_000_000_000);
};
export const encryptPassword = async (password: string): Promise<string> => {
    return await bcrypt.hash(password, pwdSaltRounds);
}
export const equalEncrypted = async (password: string, encryptedPassword: string): Promise<boolean> => {

    return await bcrypt.compare(password, encryptedPassword);
}