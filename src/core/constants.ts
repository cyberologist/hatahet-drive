// Strings

import path from 'path';

// TODO move to json to improve localization
export const unauthorizedError = 'login or create an account first.';
export const paramMissingError = 'One or more of the required parameters was missing.';
export const loginFailedErr = 'Login failed';
export const userNotFound = 'User Not Found';
export const userAlreadyExistsErr = 'User already exists';
export const internalErr = 'internal error ';
export const loginWelcomeMessage = "Welcome Again !";
export const registerWelcomeMessage = "it is pleasure :) ";
export const folderCreatedMessage = "folder created.";
export const folderDeletedMessage = "folder deleted.";
export const fileDeletedMessage = "file created.";
export const fileCreatedMessage = "file deleted.";
export const folderChildrenFetchedMessage = "folder children fetched successfully.";

// Numbers
export const pwdSaltRounds = 12;

// Cookie Properties
export const cookieProps = Object.freeze({
    key: 'ExpressGeneratorTs',
    secret: process.env.COOKIE_SECRET,
    options: {
        httpOnly: true,
        signed: true,
        path: (process.env.COOKIE_PATH),
        maxAge: Number(process.env.COOKIE_EXP),
        domain: (process.env.COOKIE_DOMAIN),
        secure: (process.env.SECURE_COOKIE === 'true'),
    },
});

// directories
export const viewsDir = path.join(path.resolve(), 'src', 'presentation', 'views');
export const staticDir = path.join(path.resolve(), 'src', 'presentation', 'public');