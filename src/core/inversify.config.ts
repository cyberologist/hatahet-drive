import "reflect-metadata";
import { Container } from "inversify";
import { RegisterUser } from '../application/usecases/register_user';
import { TYPES } from './types';
import { AuthController } from '../presentation/controllers/auth_controller';
import AuthRouter from '../presentation/routes/Auth';
import APIRouter from '../presentation/routes/api/api';
import MainRouter from '../presentation/routes/router';
import WebRouter from '../presentation/routes/web/web';
import { IUserDao } from '../domain/facades/i_user.dao';
import { LoginUser } from '../application/usecases/login_user';
import { IFolderDao } from '../domain/facades/i_folder.dao';
import { IFileDao } from '../domain/facades/i_file.dao';
import { FolderController } from '../presentation/controllers/folder_controller';
import { FileController } from '../presentation/controllers/file_controller';

import { DeleteFile } from '../application/usecases/delete_file';
import { GetFolderChildren } from '../application/usecases/get_folder_children';
import { DeleteFolder } from '../application/usecases/delete_folder';
import { CreateFile } from '../application/usecases/craete_file';
import { CreateFolder } from '../application/usecases/create_folder';
import { UserDao } from '../infrastructure/daos/user.dao';
import { FileDao } from '../infrastructure/daos/file.dao';
import { FolderDao } from '../infrastructure/daos/folder.dao';
import { JwtService } from '../application/services/JwtService';

const myContainer = new Container();


//daos
myContainer.bind<IUserDao>(TYPES.USER_DAO).to(UserDao).inSingletonScope();
myContainer.bind<IFileDao>(TYPES.FILE_DAO).to(FileDao).inSingletonScope();
myContainer.bind<IFolderDao>(TYPES.FOLDER_DAO).to(FolderDao).inSingletonScope();

//services
myContainer.bind<JwtService>(TYPES.JWT_SERVICE).to(JwtService);
//usecases
myContainer.bind<RegisterUser>(TYPES.REGISTER_USER).to(RegisterUser);
myContainer.bind<LoginUser>(TYPES.LOGIN_USER).to(LoginUser);
myContainer.bind<CreateFolder>(TYPES.CREATE_FOLDER).to(CreateFolder);
myContainer.bind<CreateFile>(TYPES.CREATE_FILE).to(CreateFile);
myContainer.bind<DeleteFolder>(TYPES.DELETE_FOLDER).to(DeleteFolder);
myContainer.bind<DeleteFile>(TYPES.DELETE_FILE).to(DeleteFile);
myContainer.bind<GetFolderChildren>(TYPES.GET_FOLDER_CHILDREN).to(GetFolderChildren);

// controllers 
myContainer.bind<AuthController>(TYPES.AUTH_CONTROLLER).to(AuthController).inSingletonScope();
myContainer.bind<FolderController>(TYPES.FOLDER_CONTROLLER).to(FolderController).inSingletonScope();
myContainer.bind<FileController>(TYPES.FILE_CONTROLLER).to(FileController).inSingletonScope();

// routers
myContainer.bind<AuthRouter>(TYPES.AUTH_ROUTER).to(AuthRouter).inSingletonScope();
myContainer.bind<APIRouter>(TYPES.API_ROUTER).to(APIRouter).inSingletonScope();
myContainer.bind<MainRouter>(TYPES.MAIN_ROUTER).to(MainRouter).inSingletonScope();
myContainer.bind<WebRouter>(TYPES.WEB_ROUTER).to(WebRouter).inSingletonScope();

export { myContainer };