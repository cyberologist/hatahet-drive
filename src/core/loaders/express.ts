
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import helmet from 'helmet';
import logger from '../utils/Logger';
import { cookieProps, staticDir, viewsDir } from '../constants';
import MainRouter from '../../presentation/routes/router';
import { TYPES } from '../types';
import { StatusCodes } from 'http-status-codes';
import { myContainer } from '../inversify.config';
import express, { Request, Response, NextFunction } from 'express';
import 'express-async-errors';


export default (app: express.Application): express.Application => {


    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(cookieParser(cookieProps.secret));

    // Show routes called in console during development
    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    }

    // Security
    if (process.env.NODE_ENV === 'production') {
        app.use(helmet());
    }

    const mainRouter: MainRouter = myContainer.get<MainRouter>(TYPES.MAIN_ROUTER);

    app.use(mainRouter.router);

    // Print API errors
    app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
        logger.error(err.message, err);
        return res.status(StatusCodes.BAD_REQUEST).json({
            error: err.message,
        });
    });

    app.set('views', viewsDir);
    app.use(express.static(staticDir));

    return app;
}
