import mongoose from 'mongoose';


export default async (): Promise<mongoose.Connection> => {
    const connection = await mongoose.connect(
        process.env.DATABASE_URL || 'database url not found', {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
    });
    return connection.connection;
};