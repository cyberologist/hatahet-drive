import dotenv from 'dotenv';
import 'reflect-metadata';

export const result2 = dotenv.config({
    path: `./env/${process.env.NODE_ENV ?? 'development'}.env`,
});

if (result2.error) {
    throw result2.error;
}
