import path from 'path';

import { Request, Response } from 'express';
import multer from 'multer';

const storage = multer.diskStorage({
    destination: function (
        req: Request, file: Express.Multer.File,
        callback: (error: Error | null, destination: string) => void
    ) {
        callback(null, path.join(path.resolve(), 'src', 'presentation', 'public', 'uploads'));
    },
    filename: function (req: Request,
        file: Express.Multer.File,
        callback: (error: Error | null, filename: string) => void
    ) {
        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
export const upload = multer({ storage: storage });


