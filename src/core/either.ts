import { exception } from 'console';

export class Either<A, B> {
    private a!: A;
    private b!: B;

    private constructor() { }

    public fold = (left: (left: A) => any, right: (right: B) => any): any => {
        if (this.a) {
            left(this.a);
        } else if (this.b) {
            right(this.b);
        }
    }

    public right = (a: A) => {
        if (this.a != null) {
            throw new Error('either value already assigned');
        }
        this.a = a;
        return this;
    }
    public left = (b: B) => {
        if (this.b != null) {
            throw new Error('either value already assigned');
        }
        this.b = b;
        return this;
    }

    public isLeft = (): boolean => this.b != null;
    public isRight = (): boolean => this.a != null;

    public getRightOrThrow = (): A => {
        if (this.a) {
            return this.a;
        } else {
            throw new Error("Value not found ");
        }

    }
    public getLeftOrThrow = (): B => {
        if (this.b) {
            return this.b;
        } else {
            throw new Error("Value not found ");
        }
    }
}