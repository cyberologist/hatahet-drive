const TYPES = {
    // daos
    USER_DAO: Symbol.for("UserDAO"),
    FOLDER_DAO: Symbol.for("FolderDao"),
    FILE_DAO: Symbol.for("FileDao"),
    //services 
    JWT_SERVICE: Symbol.for('JwtService'),
    // usecases
    REGISTER_USER: Symbol.for("RegisterUser"),
    LOGIN_USER: Symbol.for("LoginUser"),
    CREATE_FOLDER: Symbol.for("CreateFolder"),
    DELETE_FOLDER: Symbol.for("DeleteFolder"),
    CREATE_FILE: Symbol.for("CreateFile"),
    DELETE_FILE: Symbol.for("DeleteFile"),
    GET_FOLDER_CHILDREN: Symbol.for("GetFolderChildren"),
    //conrtollers
    AUTH_CONTROLLER: Symbol.for("AuthController"),
    FOLDER_CONTROLLER: Symbol.for("FileController"),
    FILE_CONTROLLER: Symbol.for("FolderController"),
    // routers
    AUTH_ROUTER: Symbol.for("AuthRouter"),
    API_ROUTER: Symbol.for("APIRouter"),
    MAIN_ROUTER: Symbol.for("MainRouter"),
    WEB_ROUTER: Symbol.for("WebRouter"),
};
export { TYPES };