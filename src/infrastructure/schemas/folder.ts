import mongoose, { Schema } from 'mongoose';
import { UserSchema } from './user';
export const FolderSchema: Schema = new mongoose.Schema({
    id: Number,
    name: {
        type: String,
        required: true,
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FolderSchema',
        required: false
    },
    owners: {
        type: [UserSchema],
        required: true
    }
})