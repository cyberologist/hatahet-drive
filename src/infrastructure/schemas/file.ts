import mongoose, { Schema } from 'mongoose';
import { FolderSchema } from './folder';
export const FileSchema: Schema = new mongoose.Schema({
    id: Number,
    name: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true,
    },
    parent: {
        type: FolderSchema,
    },
})