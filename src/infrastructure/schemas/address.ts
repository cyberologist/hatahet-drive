import mongoose, { Schema } from 'mongoose';
export const AddressSchema: Schema = new mongoose.Schema({
  lat: Number,
  long: Number,
  county: String,
  area: String,
  subArea: String,
  street: String,
});