import mongoose, { Schema } from 'mongoose';
import { AddressSchema } from './address';
import { FolderSchema } from './folder';
export const UserSchema: Schema = new mongoose.Schema({
    id: Number,
    name: String,
    phoneNumber: {
        type: String,
        required: [true, 'it is important to contact'],
        unique: true,
    },
    type: Number,
    pwdhash: String,
    image: {
        type: String,
        required: false
    },
    address: {
        type: AddressSchema,
        required: false
    },
    folders: {
        type: [FolderSchema],
        required: true
    }
})