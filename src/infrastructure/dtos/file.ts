import { File } from '../../domain/entities/File';
import { Document } from 'mongoose';
export class FileDTO extends File {

    constructor(file: File);
    constructor(mongooseDocument: Document);

    constructor(params: File | Document) {

        if (params instanceof Document) {
            super(
                params.get('_id'),
                params.get('parentId'),
                params.get('name'),
                params.get('url'),
            );
        } else {
            super(
                params.id,
                params.parentId,
                params.name,
                params.url,
            );
        }
    }
}