import { User } from '../../domain/entities/User';
import { Document } from "mongoose";
import { AddressDTO } from './address';

export class UserDTO extends User {
    constructor(user: User);
    constructor(mongooseDocument: Document);

    constructor(params: User | Document) {
        if (params instanceof Document) {
            super(
                params.get('_id'),
                params.get('name'),
                params.get('phoneNumber'),
                params.get('type'),
                params.get('pwdhash'),
                params.get('image'),
                params.get('address') && new AddressDTO(params.get('address')),
            );
        } else {
            super(
                params.id,
                params.name,
                params.phoneNumber,
                params.type,
                params.pwdhash,
                params.image,
                params.address,
            );
        }
    }

}