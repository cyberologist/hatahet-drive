import { Folder } from 'src/domain/entities/Folder';
import { Document } from 'mongoose';
export class FolderDTO extends Folder {

    constructor(file: Folder);
    constructor(mongooseDocument: Document);

    constructor(params: Folder | Document) {
        if (params instanceof Document) {
            super(
                params.get('_id'),
                params.get('name'),
                params.get('parentId')
            );
        } else {
            super(
                params.id,
                params.name,
                params.parentId,
            );
        }
    }

}