import { Address } from './../../domain/entities/Address';
import { Document } from 'mongoose';
export class AddressDTO extends Address {

    constructor(address: Address);
    constructor(mongooseDocument: Document);

    constructor(addressOrMongooseDocument: Address | Document) {
        if (addressOrMongooseDocument instanceof Document) {
            super(
                addressOrMongooseDocument.get('_id'),
                addressOrMongooseDocument.get('lat'),
                addressOrMongooseDocument.get('long'),
                addressOrMongooseDocument.get('country'),
                addressOrMongooseDocument.get('area'),
                addressOrMongooseDocument.get('subArea'),
                addressOrMongooseDocument.get('street'),
            );
        } else {
            super(
                addressOrMongooseDocument.id,
                addressOrMongooseDocument.lat,
                addressOrMongooseDocument.long,
                addressOrMongooseDocument.county,
                addressOrMongooseDocument.area,
                addressOrMongooseDocument.subArea,
                addressOrMongooseDocument.street,
            );
        }
    }
}

