import { injectable } from 'inversify';
import { User, UserType } from '../../domain/entities/User';
import { IUserDao, UserUpdateParams } from '../../domain/facades/i_user.dao';
import { UserAlreadyExistsFailure, UserNotFoundFailure, UserValueNotAvailableFailure } from '../../domain/failures/user_failures';

@injectable()
export class MockUserDao implements IUserDao {


    private users: Array<User> = [];
    constructor() { }

    async craeteUser(user: User): Promise<User | UserAlreadyExistsFailure> {

        this.users.push(user);

        return user;

    }

    async readUser(phoneNumber: string): Promise<User | UserNotFoundFailure> {

        let user = this.users.find(user => user.phoneNumber == phoneNumber);

        if ((user as User).id) {
            return user as User;
        } else {
            return new UserNotFoundFailure();
        }

    }
    async updateUser(
        phoneNumber: string,
        newInfo: UserUpdateParams
    ): Promise<User | UserNotFoundFailure | UserValueNotAvailableFailure> {

        let userIndex = this.users.findIndex(user => user.phoneNumber == phoneNumber);

        if (userIndex == -1) {

            return new UserNotFoundFailure();

        } else {

            let updatedUser: User = this.users[userIndex];

            updatedUser.phoneNumber = newInfo.phoneNumber ?? updatedUser.phoneNumber;
            updatedUser.name = newInfo.name ?? updatedUser.name;
            updatedUser.type = newInfo.type ?? updatedUser.type;
            updatedUser.pwdhash = newInfo.pwdhash ?? updatedUser.pwdhash;
            updatedUser.image = newInfo.image ?? updatedUser.image;
            updatedUser.address = newInfo.address ?? updatedUser.address;

            this.users[userIndex] = updatedUser;

            return updatedUser;
        }
    }


    async deleteUser(phoneNumber: string): Promise<User | UserNotFoundFailure> {
        let user = this.users.find(user => user.phoneNumber == phoneNumber);

        if (user === undefined) {
            return new UserNotFoundFailure();
        } else {
            this.users = this.users.filter(user => user.phoneNumber != phoneNumber);
            return user;
        }
    }

    async readUserByPhoneNumber(phoneNumber: string): Promise<User | UserNotFoundFailure> {

        let user = this.users.find(user => user.phoneNumber == phoneNumber);

        if ((user as User).id) {

            return (user as User);

        } else {

            return new UserNotFoundFailure();

        }
    }


}