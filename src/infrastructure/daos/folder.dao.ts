import { IFolderDao } from 'src/domain/facades/i_folder.dao';
import { injectable } from 'inversify';
import { FolderNotEmptyFailure, FolderNotFoundFailure, FolderNameAlreadyExistsFailure, ParentFolderNotFoundFailure } from 'src/domain/failures/folder_failures';
import { Folder } from 'src/domain/entities/Folder';
import { File } from 'src/domain/entities/File';
import mongoose from 'mongoose';
import { FolderSchema } from '../schemas/folder';
@injectable()
export class FolderDao implements IFolderDao {

    private folderModel: mongoose.Model<mongoose.Document, {}>;

    constructor() {
        this.folderModel = mongoose.model('Folder', FolderSchema);
    }
    createFolder(name: string, parentId: string, ownerId: string): Promise<Folder | FolderNameAlreadyExistsFailure | ParentFolderNotFoundFailure> {
        throw new Error("Method not implemented.");
    }
    deleteFolder(folderId: string): Promise<Folder | FolderNotEmptyFailure | FolderNotFoundFailure> {
        throw new Error("Method not implemented.");
    }
    getChildren(folderId: string, pageNumber: number): Promise<FolderNotFoundFailure | (File | Folder)[]> {
        throw new Error("Method not implemented.");
    }


}