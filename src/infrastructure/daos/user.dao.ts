
import { injectable } from 'inversify';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { User } from '../../domain/entities/User';
import { IUserDao, UserUpdateParams } from '../../domain/facades/i_user.dao';
import { UserAlreadyExistsFailure, UserNotFoundFailure, UserValueNotAvailableFailure } from '../../domain/failures/user_failures';
import { UserDTO } from '../dtos/user';
import { UserSchema } from '../schemas/user';


@injectable()
export class UserDao implements IUserDao {

    private userModel: mongoose.Model<mongoose.Document, {}>;

    constructor() {
        this.userModel = mongoose.model('User', UserSchema);
    }
    craeteUser = async (user: User): Promise<User | UserAlreadyExistsFailure> => {

        if (await this.userModel.findOne({ phoneNumber: user.phoneNumber })) {

            return new UserAlreadyExistsFailure();

        } else {

            let mongoCreatedUser = await this.userModel.create(new UserDTO(user));

            return new UserDTO(mongoCreatedUser);

        }

    }


    readUser = async (phoneNumber: string): Promise<User | UserNotFoundFailure> => {

        let userDocument: Document | null = await this.userModel.findOne({ phoneNumber: phoneNumber });

        if (userDocument == null) {

            return new UserNotFoundFailure();

        } else {

            return new UserDTO(userDocument);

        }

    }
    updateUser = async (
        userPhoneNumber: string,
        newInfo: UserUpdateParams,
    ): Promise<User | UserNotFoundFailure | UserValueNotAvailableFailure> => {

        let existingUser: Document | null =
            await this.userModel.findOne({ phoneNumber: userPhoneNumber });

        if (existingUser == null) {

            return new UserNotFoundFailure();

        } else {

            existingUser.set('phoneNumber', newInfo.phoneNumber ?? existingUser.get('phoneNumber'));
            existingUser.set('name', newInfo.name ?? existingUser.get('name'));
            existingUser.set('type', newInfo.type ?? existingUser.get('type'));
            existingUser.set('pwdhash', newInfo.pwdhash ?? existingUser.get('pwdhash'));
            existingUser.set('image', newInfo.image ?? existingUser.get('image'));
            existingUser.set('address', newInfo.address ?? existingUser.get('address'));

            let updatedUser: Document | null = await existingUser.save();

            return new UserDTO(updatedUser);
        }

    }

    deleteUser = async (phoneNumber: string): Promise<User | UserNotFoundFailure> => {

        let existingUser: Document | null =
            await this.userModel.findOne({ phoneNumber: phoneNumber });

        let deletedInformations = await this.userModel.deleteOne({ phoneNumber });

        if (
            existingUser != null &&
            deletedInformations.ok &&
            deletedInformations.deletedCount == 1
        ) {
            return new UserDTO(existingUser);
        } else {
            return new UserNotFoundFailure();

        }
    }
}

