import { IFileDao } from "src/domain/facades/i_file.dao";

import { injectable } from 'inversify';
import { FileDTO } from '../dtos/file';
import { FileNotFoundFailure, FileNameAlreadyExistsFailure, ParentFolderNotFoundFailure } from 'src/domain/failures/file_failures';

@injectable()
export class FileDao implements IFileDao {

    createFile(parentId: number, name: string, url: string): Promise<FileDTO | FileNameAlreadyExistsFailure | ParentFolderNotFoundFailure> {
        throw new Error("Method not implemented.");
    }

    deleteFile(folderId: number): Promise<FileDTO | FileNotFoundFailure> {
        throw new Error("Method not implemented.");
    }


}