export default {
    openapi: '3.0.1',
    info: {
        version: '1.0.0',
        title: 'Tajeer',
        description: 'Renting services api',
        termsOfService: 'http://api_url/terms/',
        contact: {
            name: 'Abdalrahman hataher',
            email: 'hatahet.dabest@gmail.com',
            url: 'https://www.cyberologist.com'
        },
        license: {
            name: 'Apache 2.0',
            url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
        }
    }
};