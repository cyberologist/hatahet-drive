export default {
    servers: [
        {
            url: 'http://localhost:3000/',
            description: 'Local server'
        },
        {
            url: 'https://localhost:3001',
            description: 'Testing server'
        },
        {
            url: 'https://localhost:3002',
            description: 'Production server'
        }
    ],
}