import Validator from 'validatorjs';
import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import { failureResponse } from './responser';

export const signupValidator = async (req: Request, res: Response, next: NextFunction) => {


    return validate(
        res, next, new Validator(
            req.body, {
            phone_number: 'required|size:10',
            name: 'required',
            type: 'required|in:Admin,Stantard,Owner',
            password: 'required|min:6'
        })
    );

}
export const loginValidator = async (req: Request, res: Response, next: NextFunction) => {

    return validate(
        res, next, new Validator(
            req.body, {
            phone_number: 'required',
            password: 'required'
        })
    );

}

export const getFolderChildrensValidator = async (req: Request, res: Response, next: NextFunction) => {

    return validate(
        res, next, new Validator(
            req.body, {
            folder_id: 'required|number',
            page_number: 'number'
        })
    );

}

export const createFolderValidator = async (req: Request, res: Response, next: NextFunction) => {

    return validate(
        res, next, new Validator(
            req.body, {
            parent_id: 'number',
            folder_name: 'required'
        })
    );

}

export const deleteFolderValidator = async (req: Request, res: Response, next: NextFunction) => {

    return validate(
        res, next, new Validator(
            req.body, {
            folder_id: 'required|number',
        })
    );

}

export const uploadFileValidator = async (req: Request, res: Response, next: NextFunction) => {

    return validate(
        res, next, new Validator(
            req.body, {
            parent_id: 'number|required',
            file_name: 'required'
        })
    );

}

export const deleteFileValidator = async (req: Request, res: Response, next: NextFunction) => {

    return validate(
        res, next, new Validator(
            req.body, {
            file_id: 'required|number',
        })
    );

}

const validate = (res: Response, next: NextFunction, rules: Validator.Validator<any>) => {
    if (rules.check()) {
        next();
    } else {
        return failureResponse(res, {
            code: StatusCodes.PRECONDITION_FAILED,
            message: 'validation error : ' + Object.values(rules.errors.errors)[0]
        });
    }
}
