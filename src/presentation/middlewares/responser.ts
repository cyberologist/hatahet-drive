import { StatusCodes } from 'http-status-codes';
import { Response } from 'express';
import { Failure } from 'src/domain/failures/failure';
export const successResponse = (res: Response, response?: successResponseParams) => {
    return res.status(StatusCodes.OK).json({
        success: true,
        message: response?.message ?? 'OK',
        data: response?.data ?? {}
    });
}
interface successResponseParams {
    data?: any,
    message: string
}
export const failureResponse = (res: Response, response?: failureResponseParams) => {
    return res.status(response?.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json({
        success: false,
        message: response?.message ?? 'sorry, Something went wrong ):',
    });
}
export const failureResponseFromFailure = (res: Response, failure: Failure) => {
    return failureResponse(res, {
        code: failure.statusCode,
        message: failure.message
    })
}
interface failureResponseParams {
    message: string,
    code?: StatusCodes
}

