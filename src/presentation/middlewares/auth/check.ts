import { Response, Request, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import { JwtService } from '../../../application/services/JwtService';
import { cookieProps, unauthorizedError } from '../../../core/constants';


const jwtService = new JwtService();


export const checkAuth = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const jwt = req.signedCookies[cookieProps.key];
        if (!jwt) {
            throw Error(unauthorizedError);
        }
        const clientData = await jwtService.decodeJwt(jwt);
        // TODO check exists 
        console.log(clientData);

        next();
    } catch (err) {
        return res.status(StatusCodes.UNAUTHORIZED).json({
            error: err.message,
        });
    }
};
