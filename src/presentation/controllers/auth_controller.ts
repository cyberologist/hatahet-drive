import { JwtService } from './../../application/services/JwtService';
import { UserDTO } from './../../infrastructure/dtos/user';
import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { Failure } from '../../domain/failures/failure';
import { LoginUser, LoginUserParams } from '../../application/usecases/login_user';
import { RegisterUser, RegisterUserParams } from '../../application/usecases/register_user';
import { cookieProps, loginWelcomeMessage, registerWelcomeMessage } from '../../core/constants';
import { TYPES } from '../../core/types';
import { User } from '../../domain/entities/User';
import { failureResponseFromFailure, successResponse } from '../middlewares/responser';


@injectable()
export class AuthController {
    private jwtService: JwtService;
    private loginUser: LoginUser;
    private registerUser: RegisterUser;

    constructor(
        @inject(TYPES.LOGIN_USER) loginUser: LoginUser,
        @inject(TYPES.REGISTER_USER) registerUser: RegisterUser,
        @inject(TYPES.JWT_SERVICE) jwtService: JwtService
    ) {
        this.loginUser = loginUser;
        this.registerUser = registerUser;
        this.jwtService = jwtService;
    }

    public register = async (req: Request, res: Response) => {

        const { name, phone_number, password, type } = req.body;

        let params: RegisterUserParams = new RegisterUserParams(
            name,
            phone_number,
            type,
            password
        );

        const registerResult: UserDTO | Failure = await this.registerUser.execute(params);

        if (registerResult instanceof UserDTO) {


            const jwt = await this.jwtService.getJwt(registerResult as User);

            const { key, options } = cookieProps;

            res.cookie(key, jwt, options);

            return successResponse(res, { message: registerWelcomeMessage });

        } else {

            return failureResponseFromFailure(res, registerResult as Failure);

        }
    }


    public login = async (req: Request, res: Response) => {

        const { phone_number, password } = req.body;

        let params: LoginUserParams = new LoginUserParams(phone_number, password);

        const loginResult: UserDTO | Failure = await this.loginUser.execute(params);

        if (loginResult instanceof UserDTO) {

            const jwt = await this.jwtService.getJwt(loginResult as User);

            const { key, options } = cookieProps;

            res.cookie(key, jwt, options);

            return successResponse(res, { message: loginWelcomeMessage });

        } else {

            return failureResponseFromFailure(res, loginResult as Failure);

        }
    }

    public logout = async (req: Request, res: Response) => {
        const { key, options } = cookieProps;
        res.clearCookie(key, options);
        return successResponse(res);
    }

}