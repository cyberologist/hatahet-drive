import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { CreateFolder, CreateFolderParams } from '../../application/usecases/create_folder';
import { DeleteFolder, DeleteFolderParams } from '../../application/usecases/delete_folder';
import { GetFolderChildren, GetFolderChildrenParams } from '../../application/usecases/get_folder_children';
import { Folder } from '../../domain/entities/Folder';
import { Failure } from '../../domain/failures/failure';
import { folderChildrenFetchedMessage, folderCreatedMessage, folderDeletedMessage, } from '../../core/constants';
import { TYPES } from '../../core/types';
import { failureResponseFromFailure, successResponse } from '../middlewares/responser';

@injectable()
export class FolderController {
    private createFolder: CreateFolder;
    private deleteFolder: DeleteFolder;
    private getFolderChildren: GetFolderChildren;

    constructor(
        @inject(TYPES.CREATE_FOLDER) createFolder: CreateFolder,
        @inject(TYPES.DELETE_FOLDER) deleteFolder: DeleteFolder,
        @inject(TYPES.GET_FOLDER_CHILDREN) getFolderChildren: GetFolderChildren
    ) {
        this.createFolder = createFolder;
        this.deleteFolder = deleteFolder;
        this.getFolderChildren = getFolderChildren;
    }

    public create = async (req: Request, res: Response) => {

        const { parent_id, folder_name } = req.body;

        let params: CreateFolderParams = new CreateFolderParams(parent_id, folder_name);

        const createFolderResult: Folder | Failure = await this.createFolder.execute(params);

        if (createFolderResult instanceof Folder) {

            return successResponse(res, {
                message: folderCreatedMessage,
                data: createFolderResult
            });

        } else {

            return failureResponseFromFailure(res, createFolderResult);

        }
    }
    public delete = async (req: Request, res: Response) => {

        const { folder_id } = req.body;

        let params: DeleteFolderParams = new DeleteFolderParams(folder_id);

        const createFolderResult: Folder | Failure = await this.deleteFolder.execute(params);

        if (createFolderResult instanceof Folder) {

            return successResponse(res, {
                message: folderDeletedMessage,
                data: createFolderResult
            });

        } else {
            return failureResponseFromFailure(res, createFolderResult);
        }
    }

    public getChildren = async (req: Request, res: Response) => {

        const { folder_id, page_number } = req.body;

        let params: GetFolderChildrenParams = new GetFolderChildrenParams(folder_id, page_number);

        const getFolderChildrenResult: (Folder | File)[] | Failure =
            await this.getFolderChildren.execute(params);


        if (!(getFolderChildrenResult instanceof Failure)) {

            return successResponse(res, {
                message: folderChildrenFetchedMessage,
                data: getFolderChildrenResult
            });

        } else {

            return failureResponseFromFailure(res, getFolderChildrenResult);

        }
    }


}