import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { CreateFile, CreateFileParams } from '../../application/usecases/craete_file';
import { DeleteFile, DeleteFileParams } from '../../application/usecases/delete_file';
import { fileCreatedMessage, fileDeletedMessage } from '../../core/constants';
import { Failure } from '../../domain/failures/failure';
import { TYPES } from '../../core/types';
import { failureResponseFromFailure, successResponse } from '../middlewares/responser';
import { File } from '../../domain/entities/file';
@injectable()
export class FileController {
    private createFile: CreateFile;
    private deleteFile: DeleteFile;

    constructor(
        @inject(TYPES.CREATE_FOLDER) createFolder: CreateFile,
        @inject(TYPES.DELETE_FOLDER) deleteFolder: DeleteFile,
    ) {
        this.createFile = createFolder;
        this.deleteFile = deleteFolder;
    }

    public create = async (req: Request, res: Response) => {

        const { parent_id, folder_name } = req.body;

        let params: CreateFileParams = new CreateFileParams(
            parent_id,
            req.file.filename,
            folder_name
        );

        const createFileResult: File | Failure = await this.createFile.execute(params);

        if (createFileResult instanceof File) {

            return successResponse(res, {
                message: fileCreatedMessage,
                data: createFileResult
            });

        } else {

            return failureResponseFromFailure(res, createFileResult);

        }
    }
    public delete = async (req: Request, res: Response) => {

        const { folder_id } = req.body;

        let params: DeleteFileParams = new DeleteFileParams(folder_id);

        const createFileResult: File | Failure = await this.deleteFile.execute(params);

        if (createFileResult instanceof File) {

            return successResponse(res, {
                message: fileDeletedMessage,
                data: createFileResult
            });

        } else {
            return failureResponseFromFailure(res, createFileResult);
        }
    }

}