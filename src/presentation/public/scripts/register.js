
document.addEventListener('click', function (event) {
    event.preventDefault();
    if (event.target.matches('#register-btn')) {
        var nameInput = document.getElementById('name-input');
        var phoneInput = document.getElementById('phone-input');
        var typeInput = document.getElementById('type-input');
        var pwdInput = document.getElementById('pwd-input');
        var data = {
            name: nameInput.value,
            phone_number: phoneInput.value,
            type: typeInput.value,
            password: pwdInput.value
        };
        // Default options are marked with *
        fetch('/auth/register', {
            method: 'POST',
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache',
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then((r) => {
            console.log('here');
            return r.json();
        }).then((r) => {
            console.log('rrr', r);
            var resultBox = document.getElementById('result-box');
            resultBox.textContent = r.message;
        });
        // window.location.href = '/users';
    }
}, false)
document.getElementById("login-btn").addEventListener('click', function (event) {
    event.preventDefault();
    window.location.href = '/login';

})