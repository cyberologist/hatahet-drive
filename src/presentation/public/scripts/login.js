
document.addEventListener('click', function (event) {
    event.preventDefault();
    if (event.target.matches('#login-btn')) {
        var phoneInput = document.getElementById('phone-input');
        var pwdInput = document.getElementById('pwd-input');
        var data = {
            phone_number: phoneInput.value,
            password: pwdInput.value
        };
        Http.Post('/auth/login', data)
            .then((res) => {
                return res.json();
            }).then((res) => {
                var resultBox = document.getElementById('result-box');
                resultBox.textContent = res.message;
                console.log(res);
            })
        // window.location.href = '/users';
    }
}, false)
document.getElementById("register-btn").addEventListener('click', function (event) {
    event.preventDefault();
    window.location.href = '/register';

})