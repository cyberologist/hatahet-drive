import { Router } from 'express';
import { inject, injectable } from 'inversify';
import { TYPES } from '../../core/types';
import { AuthController } from '../controllers/auth_controller';
import { loginValidator, signupValidator } from '../middlewares/validators';

@injectable()
class AuthRouter {
    private _router: Router;
    private authController: AuthController;
    constructor(
        @inject(TYPES.AUTH_CONTROLLER) authController: AuthController
    ) {
        this._router = Router();
        this.authController = authController;
        /******************************************************************************
        *                      Login User - "POST /api/auth/login"
        ******************************************************************************/
        this._router.post('/register', signupValidator, authController.register);

        /******************************************************************************
        *                      Login User - "POST /api/auth/login"
        ******************************************************************************/
        this._router.post('/login', loginValidator, authController.login);
        /******************************************************************************
         *                      Logout - "GET /api/auth/logout"
         ******************************************************************************/
        this._router.get('/logout', authController.logout);

    }
    get router(): Router { return this._router };
}

export default AuthRouter;

