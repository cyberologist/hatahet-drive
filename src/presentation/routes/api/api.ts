import { Router } from 'express';
import { inject, injectable } from 'inversify';
import { FolderController } from '../../../presentation/controllers/folder_controller';
import { TYPES } from '../../../core/types';
import {
    createFolderValidator, deleteFileValidator, getFolderChildrensValidator, uploadFileValidator
} from '../../../presentation/middlewares/validators';
import { upload } from '../../../core/loaders/multer';
import { FileController } from '../../../presentation/controllers/file_controller';
import { checkAuth } from 'src/presentation/middlewares/auth/check';



@injectable()
class APIRouter {
    private _router: Router;
    private folderController: FolderController;
    private fileController: FileController;

    constructor(
        @inject(TYPES.FOLDER_CONTROLLER) folderController: FolderController,
        @inject(TYPES.FILE_CONTROLLER) fileController: FileController
    ) {

        this.folderController = folderController;

        this.fileController = fileController;

        this._router = Router();

        /******************************************************************************
        *                      Create a folder - "POST /api/folder"
        ******************************************************************************/
        this._router.post('/folder', checkAuth, createFolderValidator, folderController.create);
        /******************************************************************************
        *                      Get folder Children - "Get /api/folder"
        ******************************************************************************/
        this._router.post('/folder', checkAuth, getFolderChildrensValidator, folderController.getChildren);    /******************************************************************************
            *                      Delete folder - "DELETE /api/folder"
            ******************************************************************************/
        this._router.post('/folder', checkAuth, createFolderValidator, folderController.delete);
        /******************************************************************************
        *                      Upload a file - "POST /api/file"
        ******************************************************************************/
        this._router.post('/file', checkAuth, upload.single('file'), uploadFileValidator, fileController.create);
        /******************************************************************************
        *                      Get folder Children - "Get /api/folder"
        ******************************************************************************/
        this._router.post('/file', checkAuth, deleteFileValidator, fileController.delete);

    }
    get router(): Router { return this._router };
}

export default APIRouter;
