import { Router } from 'express';
import { inject, injectable } from 'inversify';
import AuthRouter from './Auth';
import APIRouter from './api/api';
import WebRouter from './web/web';
import { TYPES } from '../../core/types';
import { checkAuth } from '../middlewares/auth/check';


@injectable()
class MainRouter {
    private authRouter: AuthRouter;
    private apiRouter: APIRouter;
    private webRouter: WebRouter;
    private _router: Router;
    constructor(
        @inject(TYPES.AUTH_ROUTER) authRouter: AuthRouter,
        @inject(TYPES.API_ROUTER) apiRouter: APIRouter,
        @inject(TYPES.WEB_ROUTER) webRouter: WebRouter
    ) {
        this._router = Router();
        this.authRouter = authRouter;
        this.apiRouter = apiRouter;
        this.webRouter = webRouter;

        this._router.use('/auth', authRouter.router);
        this._router.use('/api', checkAuth, apiRouter.router);
        this._router.use('/', webRouter.router);
    }
    get router(): Router { return this._router };
}

export default MainRouter;
