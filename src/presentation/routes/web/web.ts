import { Router } from 'express';
import { injectable } from 'inversify';
import { Request, Response } from 'express';
import { cookieProps, viewsDir } from '../../../core/constants';

import { openApiDocumentation } from '../../docs/open_api_documentation';
import * as swaggerUi from 'swagger-ui-express';
@injectable()
class WebRouter {
    private _router: Router;
    constructor() {
        this._router = Router();

        /******************************************************************************
        *                      Show API Docs - "Get /api/docs"
        ******************************************************************************/
        this._router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openApiDocumentation));

        /******************************************************************************
        *                      Login User - "Get /login"
        ******************************************************************************/

        this._router.get('/login', (req: Request, res: Response) => {
            res.sendFile('login.html', { root: viewsDir });
        });

        /******************************************************************************
        *                      Login User - "Get /login"
        ******************************************************************************/
        this._router.get('/', (req: Request, res: Response) => res.redirect('/login'));

        /******************************************************************************
        *                      Register User - "Get /register"
        ******************************************************************************/
        this._router.get('/register', (req: Request, res: Response) => {
            res.sendFile('register.html', { root: viewsDir });
        });

        /******************************************************************************
        *                      Get All users - "Get /users"
        ******************************************************************************/
        this._router.get('/users', (req: Request, res: Response) => {
            const jwt = req.signedCookies[cookieProps.key];
            if (!jwt) {
                res.redirect('/');
            } else {
                res.sendFile('users.html', { root: viewsDir });
            }
        });

    }
    get router(): Router { return this._router };
}

export default WebRouter;
