import './core/loaders/LoadEnv'; // Must be the first import
import App from './app';

const port = Number(process.env.PORT || 3000);
App.getInstance().listen(port);
