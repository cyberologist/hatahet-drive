import './core/loaders/LoadEnv'; // Must be the first import
import express from 'express';
import mongooseLoader from './core/loaders/mongoose';
import expressLoader from './core/loaders/express';
import mongoose from 'mongoose';
import logger from './core/utils/Logger';

export default class App {

    private mongooseConnection!: mongoose.Connection;
    private expressApp!: express.Application;
    private static _instance: App;
    private constructor() { };

    public static getInstance = (): App => {
        if (App._instance == null) {
            App._instance = new App();
        }
        return App._instance;
    }

    get server(): Promise<express.Application> {
        return new Promise(async resolve => {
            await this.connect();
            resolve(this.expressApp);
        });
    };

    get dbConnection(): Promise<mongoose.Connection> {
        return new Promise(async resolve => {
            if (!this.mongooseConnection) {
                await this.connectMongoose();
            }
            resolve(this.mongooseConnection);
        });
    };

    public connectExpress = async (): Promise<void> => {
        this.expressApp = await expressLoader(express());
        logger.info('Express Loaded Successfully');
    }

    public connectMongoose = async (): Promise<void> => {
        this.mongooseConnection = await mongooseLoader();
        logger.info('mongoose DB Loaded And Connected');
    }

    public connect = async (): Promise<express.Application> => {
        if (!this.mongooseConnection) {
            await this.connectMongoose();
        }
        if (!this.expressApp) {
            await this.connectExpress();
        }
        return this.expressApp;
    }

    public listen = async (port: number) => {
        (await this.connect()).listen(port);
        logger.info('server listening on port ' + port);
    }

    public disconnect = async () => {
        this.mongooseConnection.close();
    }

} 
